package runtime

import (
	"github.com/docker/docker/client"
	"github.com/pkg/errors"
)

type Runtime struct {
	cli *client.Client
}

func NewRuntime() (*Runtime, error) {
	cli, err := client.NewClientWithOpts(client.FromEnv)
	if err != nil {
		return nil, errors.Wrap(err, "could not create docker client")
	}

	return &Runtime{cli}, nil
}
