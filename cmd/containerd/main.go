package main

import (
	"context"
	"fmt"
	"io"
	"os"

	"containerd/internal/runtime"

	"github.com/pkg/errors"
)

const CONFIG_FILE = "config.yml"

func fatal(err error, format string, a ...any) {
	err = errors.Wrapf(err, format, a...)
	fmt.Fprintf(os.Stderr, "[ERROR] %v\n", err)
	os.Exit(1)
}

func main() {
	fmt.Println("[INFO] creating runtime")
	rt, err := runtime.NewRuntime()
	if err != nil {
		fatal(err, "failed to create runtime")
	}
	fmt.Println("[INFO] runtime created")

	ctx := context.Background()

	fmt.Printf("[INFO] creating pipeline from configuration %s\n", CONFIG_FILE)
	p, err := rt.NewPipeline("config.yml")
	if err != nil {
		fatal(err, "failed to create pipeline from configuration %s", CONFIG_FILE)
	}
	fmt.Printf("[INFO] pipeline created from configuration %s\n", CONFIG_FILE)

	fmt.Println("[INFO] starting pipeline")
	logs, errs := p.Start(ctx)
	if len(errs) > 0 {
		fmt.Fprintf(
			os.Stderr,
			"[ERROR] failed to start pipeline %s: %d errors\n\n",
			p.ID.Short(),
			len(errs),
		)
		for name, err := range errs {
			fmt.Fprintf(os.Stderr, "- [ERROR][%s] %v\n\n", name, err)
		}
		os.Exit(1)
	}

	for _, log := range logs {
		fmt.Printf("\n--------- %s Job Logs (%s) ---------\n", log.JobName, log.JobID.Short())
		_, err = io.Copy(os.Stdout, log.Stdcmb)
		if err != nil {
			fatal(err, "failed to copy stdcmb logs %s to stdout", log.JobName)
		}
		fmt.Println("EOF")
	}

	fmt.Println("[INFO] pipeline cleanup")
	err = p.Cleanup(ctx)
	if err != nil {
		fatal(err, "failed to cleanup pipeline")
	}
	fmt.Println("[INFO] done")
}
